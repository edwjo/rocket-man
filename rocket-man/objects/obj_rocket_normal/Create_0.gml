

INIT_SPEED = 20;
MAX_SPEED = 100;

acc = 1;
spd = 0;
dir = facing.RIGHT;
outside_room = false;
particles = instance_create_layer(x, y, "Instances", obj_rocket_particles);
particles.rocket = self;
particles_offset = new Vector2(0, 0);


/// @function						set_dir(_dir);
///	@desc							Use to set the direction of the rocket.
/// @param	{facing}	_dir		The direction of the rocket. See enum [facing].
set_dir = function(_dir) {
	dir = _dir;
	switch(_dir) {
		case facing.RIGHT:
			spd = INIT_SPEED + obj_player.x_speed;
			particles_offset = new Vector2(-64, 32);
			break;
		case facing.LEFT: 
			acc = -acc;
			spd = -INIT_SPEED + obj_player.x_speed;
			image_xscale = -1;
			particles_offset = new Vector2(64, 32);
			break;
		case facing.DOWN: 
			x += 64;
			image_angle = 270;
			spd = INIT_SPEED + obj_player.y_speed + 10;
			particles_offset = new Vector2(-28, -64);
			break;
		case facing.UP:
			image_angle = 90;
			acc = -acc;
			spd = (-INIT_SPEED + obj_player.y_speed > 0 ? 0 : -INIT_SPEED + obj_player.y_speed);
			particles_offset = new Vector2(28, 64);
			break;
	}
}

/// @function					get_particles_pos();
/// @desc						Used by obj_rockets_particles to get it's region pos.
get_particles_pos = function() {
	return new Vector2(x + particles_offset.x, y + particles_offset.y);
}

