/*var inst = instance_place(x, y, obj_door_trigger);
if (inst != noone) {
	inst.trigger();
}*/

if (place_meeting(x, y, obj_wall)) {
	instance_destroy();
}

spd += acc;
if (abs(spd) > MAX_SPEED) {
	spd = sign(spd)*MAX_SPEED;
}

if (dir == facing.LEFT || dir == facing.RIGHT) {
	x += spd;
}
else {
	y += spd;
}

