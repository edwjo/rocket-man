/// @description create blast, reload
obj_player.reload();
particles.begin_destruction();
if (!outside_room) {
	obj_camera.shake_camera();
	switch (dir) {
		case facing.RIGHT:
			instance_create_layer(x + 32, y + 32, "Instances", obj_rocket_blast);
			break;
		case facing.LEFT:
			instance_create_layer(x - 32, y + 32, "Instances", obj_rocket_blast);
			break;
		case facing.DOWN:
			instance_create_layer(x - 32, y + 32, "Instances", obj_rocket_blast);
			break;
		case facing.UP:
			instance_create_layer(x + 32, y - 32, "Instances", obj_rocket_blast);
			break;
	}
}


