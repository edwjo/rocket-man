

INIT_SPEED = 0;
MAX_SPEED = 5;

acc = 0.03;
spd = 0;
dir = facing.RIGHT;


/// @function						set_dir(_dir);
///	@desc							Use to set the direction of the rocket.
/// @param	{facing}	_dir		The direction of the rocket. See enum [facing].
set_dir = function(_dir) {
	dir = _dir;
	switch(_dir) {
		case facing.RIGHT:
			spd = INIT_SPEED + obj_player.x_speed;
			x += sprite_width;
			break;
		case facing.LEFT: 
			acc = -acc;
			spd = -INIT_SPEED + obj_player.x_speed;
			image_xscale = -1;
			break;
		case facing.DOWN: 
			image_angle = 270;
			spd = INIT_SPEED + obj_player.y_speed + 10;
			break;
		case facing.UP:
			image_angle = 90;
			acc = -acc;
			spd = (-INIT_SPEED + obj_player.y_speed > 0 ? 0 : -INIT_SPEED + obj_player.y_speed);
			break;
	}
}
