PADDING = 8;

system = part_system_create();

part = part_type_create();
part_type_sprite(part, spr_destructible_block_part, false, false, false);
part_type_life(part, 0.2*room_speed, 0.5*room_speed);
part_type_alpha2(part, 1, 0.6);
part_type_size(part, 1, 5, -0.05, 0);
part_type_color_mix(part, $E5E5E5, $999999);
part_type_gravity(part, 0.15, 270);
part_type_orientation(part, 0, 359, 3, 0.1, true);

emitter = part_emitter_create(system);
part_emitter_region(
	system,
	emitter,
	x + PADDING,
	x + sprite_get_width(spr_destructible_block) - PADDING,
	y + PADDING,
	y + sprite_get_height(spr_destructible_block) - PADDING,
	ps_shape_rectangle,
	ps_distr_linear
);
part_emitter_burst(system, emitter, part, 9);

alarm[0] = room_speed;
