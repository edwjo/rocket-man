/// @description 

alpha = clamp(alpha + fade_dir/(fade_time*room_speed), 0, 1);

if (alpha == 1 && fade_dir == 1) {
	dark_counter++;
	if (dark_counter > dark_time*room_speed) {
		room_goto(dest_room);
		if (dest_pos.x != -1 && dest_pos.y != -1) {
			obj_player.x = dest_pos.x;
			obj_player.y = dest_pos.y;
			obj_player.state = player_state.ALIVE;
		}
		fade_dir = -1;
	}
}
else if (alpha == 0 && fade_dir == -1) {
	
	instance_destroy();
}

