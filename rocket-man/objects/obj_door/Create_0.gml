//SEE VARIABLE DEF TAB

alpha = 0;
fade_dir = 1;
dark_counter = 0;
transitioning = false;


/// @function		start_transition();
/// @desc			Transition to [dest_room] by instantiating an obj_transition instance if door is unlocked.
start_transition = function() {
	var transition = instance_create_layer(0, 0, "Instances", obj_transition);
	transition.dest_pos = dest_pos;
	transition.dest_room = dest_room;
}

/// @function		unlock();
unlock = function() {
	locked = false;
	image_index = 0;
}

/// @function		lock();
lock = function() {
	locked = true;
	image_index = 1;
}
