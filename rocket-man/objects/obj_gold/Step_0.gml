if (place_meeting(x, y, obj_player) && transition == noone) {
	transition = instance_create_layer(0, 0, "Instances", obj_transition);
	transition.dest_room = rm_outro;
	obj_player.persistent = false;
}