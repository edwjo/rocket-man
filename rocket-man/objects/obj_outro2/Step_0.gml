skip = mouse_check_button_pressed(mb_left) || keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space) || keyboard_check_pressed(ord("E"));

char_counter++;
if (char_counter >= room_speed/draw_speed && char_counter < string_length(text)) {
	char_counter = 0;
	char_index++;
}

if (skip) {

	if (char_index < string_length(text)) {
		char_index = string_length(text)-1;
	}
	else {
		game_end();
	}
}

