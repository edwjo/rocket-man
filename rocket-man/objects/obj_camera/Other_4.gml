/// @description Set camera props
if (instance_exists(obj_player)) {
	x = obj_player.x;
	y = obj_player.y;
}
else {
	x = 0;
	y = 0;
}


var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
var pm = matrix_build_projection_ortho(CAMERA_WIDTH, CAMERA_HEIGHT, 1, 10000);

camera_set_view_mat(camera, vm);
camera_set_proj_mat(camera, pm);

view_camera[0] = camera;


