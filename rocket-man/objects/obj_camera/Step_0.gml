if (instance_exists(follow)) {
	dest_pos = follow.get_camera_dest_pos();
}

//move toward the player
x += (dest_pos.x - x)/15;
y += (dest_pos.y - y)/15;

x = clamp(x, CAMERA_WIDTH/2, room_width - CAMERA_WIDTH/2);
y = clamp(y, CAMERA_HEIGHT/2, room_height - CAMERA_HEIGHT/2);

if (shake) {
	//add shake to camera pos (can go beyond room borders)
	x += irandom_range(-SHAKE_RANGE, SHAKE_RANGE);
	y += irandom_range(-SHAKE_RANGE, SHAKE_RANGE);

	//update camera
	var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
	camera_set_view_mat(camera, vm);
	
	//clamp the values so camera is put inside room borders
	x = clamp(x, CAMERA_WIDTH/2, room_width - CAMERA_WIDTH/2);
	y = clamp(y, CAMERA_HEIGHT/2, room_height - CAMERA_HEIGHT/2);
}
else {
	var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
	camera_set_view_mat(camera, vm);
}