CAMERA_WIDTH = 1920;
CAMERA_HEIGHT = 1080;
SHAKE_RANGE = 4;

follow = obj_player;
dest_pos = new Vector2(x, y);
shake = false;
camera = camera_create();


/// @function		shake_camera();
shake_camera = function() {
	shake = true;
	alarm[0] = 0.15*room_speed;
}