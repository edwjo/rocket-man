MAX_LASERS_SCALE = 100;

var laser_scale = 1;
var laser = instance_create_layer(x, y+64, "Instances", obj_laser_beam);
laser.sprite_index = laser_sprite;
laser.image_index = laser_cycle mod laser.image_number;
laser.image_speed = laser.image_number/cycle_time;

if (dir == facing.RIGHT || dir == facing.LEFT) {
	image_angle = 90;
	y += sprite_height;
	
	with (laser) {
		image_angle = 90;
	}
}

var break_loop = false;
while (laser_scale < MAX_LASERS_SCALE) {
	switch (dir) {
		case facing.UP:
		case facing.DOWN:
			if (place_meeting(x, y - laser_scale*laser.sprite_height, obj_laser_receiver)) {
				break_loop = true;
			}
			break;
		case facing.RIGHT:
		case facing.LEFT:
			if (place_meeting(x + laser_scale*laser.sprite_width, y, obj_laser_receiver)) {
				break_loop = true;
			}
			break;
		default:
			show_error("Wrong value in switch statement", false);
	}
	if (break_loop) {
		break;
	}
	laser_scale++;
}

switch (dir) {
	case facing.UP:
	case facing.DOWN:
		laser.image_yscale = -(laser_scale + 1);
		break;
	case facing.RIGHT:
	case facing.LEFT:
		laser.image_yscale = laser_scale + 1;
		break;
}
