/// @description collision with player
time += 1/room_speed;
if (time >= 2*3.14) {
	time -= 2*3.14;
}
if (place_meeting(x, y, obj_player) && obj_player.dead_on_explosion == true) {
	obj_player.dead_on_explosion = false;
	should_draw = true;
	alarm[0] = DISPLAY_TIME*room_speed;
	obj_sound.play_sound_pitch(snd_powerup, 2);
}

if (is_fading) {
	alpha = clamp(alpha - 1/(FADE_TIME*room_speed), 0, 1);
	if (alpha == 0) {
		should_draw = false;
	}
}