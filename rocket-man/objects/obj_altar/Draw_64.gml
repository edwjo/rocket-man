/// @description power up description

if (should_draw) {
	draw_set_alpha(alpha);
	draw_set_color(c_black);
	draw_rectangle(0, 0, display_get_gui_width(), BOX_HEIGHT, false); 
	
	/*
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_set_color(c_white);
	draw_set_font(fnt_power_up);
	draw_text(display_get_gui_width()/2, BOX_HEIGHT/2, "Explosion damage resistance aquired");*/
	draw_sprite_ext(
		spr_shield,
		0,
		display_get_gui_width()/2,
		BOX_HEIGHT/2,
		2.7 + SCALE_AMPLITUDE*sin(2*3.14*SCALE_FREQ*time),
		2.7 + SCALE_AMPLITUDE*sin(2*3.14*SCALE_FREQ*time),
		0,
		-1,
		alpha
	);
	
	draw_set_alpha(1);
}