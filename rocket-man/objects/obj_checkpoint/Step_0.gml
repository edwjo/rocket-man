/// @description collision with player
if (place_meeting(x, y, obj_player) && !captured) {
	image_index = 1;
	captured = true;
	obj_player.spawn_point = new Vector2(x, y);
	
	if (visible) {
		obj_sound.play_sound(snd_checkpoint);
	}
	
	if (!instance_exists(obj_respawn_reset)) {
		instance_create_layer(0, 0, "Instances", obj_respawn_reset);
	}
	obj_respawn_reset.add_door_triggers();
}