/// @description set sprite

switch (triggers_needed) {
	case 1:
		sprite_index = spr_door_display_one;
		break;
	case 2:
		sprite_index = spr_door_display_two;
		break;
	case 3:
		sprite_index = spr_door_display_three;
		break;
	default:
		show_error("Faulty value in variable [triggers_needed] in obj_door_display", true);
}