//SEE VARIABLE DEFINITION TAB

trigger_count = 0;
draw_white = false;

/// @function		increment_triggers();
increment_triggers = function() {
	trigger_count++;
	draw_white = true;
	image_index++;
	if (trigger_count >= triggers_needed) {
		target_door.unlock();
	}
}

/// @function		decrement_triggers();
decrement_triggers = function() {
	if (trigger_count > 0) {
		trigger_count--;
		image_index--;
	}

	if (trigger_count < triggers_needed) {
		target_door.lock();
	}
}

