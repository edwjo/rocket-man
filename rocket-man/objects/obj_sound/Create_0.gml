audio_group_load(sound_effects);
audio_group_load(background_music);

play_sound = function(sound) {
	audio_play_sound(sound, 1, false);
}

play_sound_pitch = function(sound, pitch) {
	var snd = audio_play_sound(sound, 1, false);
	audio_sound_pitch(snd, pitch);
}