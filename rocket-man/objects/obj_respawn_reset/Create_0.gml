door_triggers = ds_list_create();

reset = function() {
	instance_activate_object(obj_destructible_block);
	with(obj_destructible_block) {
		alarm[0] = -1;	
	}
	
	with(obj_door_trigger) {
		if (ds_list_find_index(other.door_triggers, self) == -1) {
			reset();
		}
	}
}

add_door_triggers = function() {
	with(obj_door_trigger) {
		if (is_triggered && ds_list_find_index(other.door_triggers, self) == -1) {
			ds_list_add(other.door_triggers, self);
		}
	}	
}