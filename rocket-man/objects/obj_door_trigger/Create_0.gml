//see variable definition tab

is_triggered = false;

//set sprite direction
switch (dir) {
	case facing.RIGHT:
		image_angle = -90;
		x += sprite_width;
		break;
	case facing.LEFT:
		image_angle = 90;
		y += sprite_height;
		break;
	case facing.DOWN:
		image_angle = 180;
		x += sprite_width;
		y += sprite_height;
		break;
}

/// @function		reset();
reset = function() {

	
	if (!instance_exists(target) || !is_triggered) {
		return;
	}
	if (target.object_index == obj_door_display) {
		target.decrement_triggers();
	}
	
	is_triggered = false;
	image_index = 0;
}

/// @function		trigger();
trigger = function() {
	if (!is_triggered) {
		image_index = 1;
		is_triggered = true;


		if (!instance_exists(target)) {
			return;
		}
		//perform action depending on target
		switch (target.object_index) {
			case obj_door:
				target.unlock();
				break;
			case obj_door_display:
				target.increment_triggers();
				break;
			default:
				show_error("Instance id in variable [target] has faulty object type in obj_door_trigger.", true);
		}
	}
}