camera_x = obj_camera.x - view_wport[0];
camera_y = obj_camera.y - view_hport[0];

var set_bg_pos = function(layer_name, lerp_amount) {
	var layer_id = layer_get_id(layer_name);
	if (layer_id != -1) {
		layer_x(layer_id, lerp(0, camera_x, lerp_amount));
		layer_y(layer_id, camera_y);
	}
}

set_bg_pos("bg", 1);
set_bg_pos("bg_far", 0.8);
set_bg_pos("bg_near", 0.7);



