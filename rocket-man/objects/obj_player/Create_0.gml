/// @description

enum player_state {
	ALIVE,
	DEAD,
	TRANSITIONING_ROOM
}

WALK_SPEED_MAX = 5;
WALK_SPEED_MAX_AIR = 7;
FALL_SPEED_MAX = 20;
JUMP_SPEED = 9.8;
ROCKET_JUMP_X_SPEED_MAX = 15;
ROCKET_JUMP_Y_SPEED_MAX = 33;
HOLD_TIME_MAX = 0.3; //seconds
X_ACC = 0.5;
Y_ACC = 1.0;
Y_ACC_HOLDING = 0.25;
DUST_FREQUENCY = 30; //seconds
DUST_BURST_COUNT = 2;
FIRE_FREQUENCY = 30;
FIRE_BURST_COUNT = 5;
RESPAWN_TIME = 0.7;
ROCKET_ARSENAL = [obj_rocket_normal];
ANGEL_Y_SPEED = 5;
ANGEL_ALPHA = 0.6;
WHITE_TIME = 1/room_speed;

x_speed = 0;
y_speed = 0;
x_speed_max = WALK_SPEED_MAX;
y_speed_max = FALL_SPEED_MAX;
holding_up = false;
x_facing = facing.RIGHT;
can_shoot = true;
rocket_jumping = false;
grounded = true;
grounded_prev = grounded;
x_dir = 0;
y_dir = 0;
image_xscale_prev = image_xscale;
dust_time_counter = 0;
fire_time_counter = 0;
rocket_index = 0;
state = player_state.ALIVE;
rocket = noone;
up_press = false;
space_pressed = false;
interact_pressed = false;
dead_on_explosion = true;
spawn_point = new Vector2(-1, -1);
draw_white = false;

//particle system
dust_system = part_system_create();
fire_system = part_system_create();
//particle type
//dust particle
dust_part = part_type_create();
part_type_sprite(dust_part, spr_dust_particle, false, false, false);
part_type_life(dust_part, 0.5*room_speed, room_speed);
part_type_alpha2(dust_part, 0.8, 0.5);
part_type_size(dust_part, 0.2, 1, -0.04, 0);
part_type_color_mix(dust_part, $E5E5E5, $999999);
//fire particle
fire_part = part_type_create();
part_type_sprite(fire_part, spr_fire_particle, false, false, false);
part_type_life(fire_part, 0.8*room_speed, 1.5*room_speed);
part_type_alpha2(fire_part, 0.9, 0.5);
part_type_size(fire_part, 0.3, 1.5, -0.04, 0.08);
part_type_color_mix(fire_part, c_red, c_orange);

//particle emitter
dust_emitter = part_emitter_create(dust_system);
fire_emitter = part_emitter_create(fire_system);

/// @function					die();
///	@desc						Call when player dies.
die = function() {
	if (state == player_state.ALIVE) {
		state = player_state.DEAD;
		x_speed = 0;
		y_speed = -ANGEL_Y_SPEED;
		image_alpha = ANGEL_ALPHA;
		sprite_index = spr_player_dead;
		alarm[1] = RESPAWN_TIME*room_speed;
		obj_sound.play_sound_pitch(snd_death, 2);
	}
}

/// @function					get_camera_dest_pos();
/// @desc						Get the destination position of where the camera
								//wants to be placed. Dependent on player speeds.
/// @ret	{Vector2}			The destination position for the camera. 
get_camera_dest_pos = function() {
	var dest_x = x + sprite_width/2 + x_speed*30;
	var dest_y = y + sprite_height/2 + y_speed*15;
	return new Vector2(dest_x, dest_y);
}

/// @function					set_explosion_speed(x_speed, y_speed);
/// @desc						Set the speed generated from a rocket explosion.
/// @param	{real}	_x_speed	The x speed to apply to this player.
/// @param	{real}	_y_speed	The y speed to apply to this player.
set_explosion_speed = function(_x_speed, _y_speed) {
	x_speed = _x_speed;
	y_speed = _y_speed;
	
	x_speed_max += abs(_x_speed);
	if (x_speed_max > ROCKET_JUMP_X_SPEED_MAX) {
		x_speed_max = ROCKET_JUMP_X_SPEED_MAX;
	}
	
	y_speed_max += abs(_y_speed);
	if (y_speed_max > ROCKET_JUMP_Y_SPEED_MAX) {
		y_speed_max = ROCKET_JUMP_Y_SPEED_MAX;
	}

	rocket_jumping = true;
}

/// @function					reload();
/// @desc						Reload the rocket launcher.
reload = function() {
	can_shoot = true;
}

/// @function					get_center_x();
get_center_x = function() {
	return x + sprite_width/2;
}

/// @function					get_center_y();
get_center_y = function() {
	return y + sprite_height/2;
}

/// @function					set_sprite();
set_sprite = function() {
	//set sprite
	if (y_dir == 1) {
		if (x_facing == facing.RIGHT) {
			sprite_index = spr_player_down;
		}
		else if (x_facing == facing.LEFT) {
			sprite_index = spr_player_back_down;
		}
	}
	else if (y_dir == -1) {
		if (x_facing == facing.RIGHT) {
			sprite_index = spr_player_up;
		}
		else if (x_facing == facing.LEFT) {
			sprite_index = spr_player_back_up;
		}
	}
	else {
		if (x_facing == facing.RIGHT) {
			sprite_index = spr_player_forward;
		}
		else if (x_facing == facing.LEFT) {
			sprite_index = spr_player_back_forward;
		}
	}

	//set indexes
	if (!grounded) {
		image_index = 1;
	}
	else if (x_dir == 0 || (grounded_prev == false && grounded == true)) {
		image_index = 0;
	}
}

/// @function					handle_fire_particles();
/// @desc						Handle the fire particles, created when rocket jumping. 
handle_fire_particles = function() {
	fire_time_counter++;
	if (fire_time_counter >= room_speed/FIRE_FREQUENCY) {
		fire_time_counter = 0;
		if (rocket_jumping) {
			part_emitter_region(
				fire_system, 
				fire_emitter, 
				x + 8, 
				x + sprite_width - 8, 
				y + 8, 
				y + sprite_height - 8, 
				ps_shape_ellipse, 
				ps_distr_gaussian
			);
			part_emitter_burst(fire_system, fire_emitter, fire_part, FIRE_BURST_COUNT);
		}
	}
}

/// @function					handle_dust_particles();
/// @desc						Handle the dust particles generated when running and landing. 
								//For dust particles generated from jumping, see function 
								//handle_dust_particles_jumping();
handle_dust_particles = function() {
	dust_time_counter++;
	//dust particles from walking 
	if (dust_time_counter >= room_speed/DUST_FREQUENCY) {
		dust_time_counter = 0;
		if (grounded && abs(x_speed) == x_speed_max) {
			var x_min = (x_facing == facing.RIGHT ? x + 4 : x + sprite_width/2);
			var x_max = (x_facing == facing.RIGHT ? x + sprite_width/2 : x + sprite_width - 4);
			part_emitter_region(
				dust_system, 
				dust_emitter, 
				x_min, 
				x_max, 
				y + sprite_height - 4, 
				y + sprite_height, 
				ps_shape_ellipse, 
				ps_distr_gaussian
			);
			part_emitter_burst(dust_system, dust_emitter, dust_part, DUST_BURST_COUNT);
		}
	}
	
	//dust particles from landing on ground
	if (grounded && !grounded_prev) {
		//burst dust particles in the air
		part_emitter_region(
			dust_system, 
			dust_emitter, 
			x - 8, 
			x + sprite_width + 8, 
			y + sprite_height - 32, 
			y + sprite_height, 
			ps_shape_rectangle, 
			ps_distr_gaussian
		);
		part_emitter_burst(dust_system, dust_emitter, dust_part, 5*DUST_BURST_COUNT);
	}
}

/// @function					handle_dust_particles_jumping();
/// @desc						Handle the dust particles generated when jumping. 
handle_dust_particles_jumping = function() {
	//burst dust particles in the air
	part_emitter_region(
		dust_system, 
		dust_emitter, 
		x + 16, 
		x + sprite_width - 16, 
		y + sprite_height - 48, 
		y + sprite_height, 
		ps_shape_ellipse, 
		ps_distr_gaussian
	);
	part_emitter_burst(dust_system, dust_emitter, dust_part, 3*DUST_BURST_COUNT);
}

/// @function					shoot();
/// @desc						Shoot rocket ROCKET_ARSENAL[rocket_index];
shoot = function() {
	can_shoot = false;
	rocket = instance_create_layer(x, y, "Instances", ROCKET_ARSENAL[rocket_index]);
	if (y_dir == -1) {
		rocket.set_dir(facing.UP);
	}
	else if (y_dir == 1) {
		rocket.set_dir(facing.DOWN);
	} 
	else {
		rocket.set_dir(x_facing);
	}
}

/// @function					set_x_facing();
set_x_facing = function() {
	if (x_dir == 1) {
		x_facing = facing.RIGHT;
	}
	else if (x_dir == -1) {
		x_facing = facing.LEFT;
	}
}

/// @function					handle_x_collision();
handle_x_collision = function() {
	if (place_meeting(x + x_speed, y, obj_wall)) {
		while (!place_meeting(x + sign(x_speed), y, obj_wall)) {
			x += sign(x_speed);
		}
		x_speed = 0;
	}
}

/// @function					handle_y_collision();
handle_y_collision = function() {
	if (place_meeting(x, y + y_speed, obj_wall)) {
		while (!place_meeting(x, y + sign(y_speed), obj_wall)) {
			y += sign(y_speed);
		}
		//fix for player possibly getting stuck in corner of wall
		if (place_meeting(x, y, obj_wall)) {
			y -= y_speed; 
		}
		y_speed = 0;
	}
}

/// @function					get_keyboard_inputs();
get_keyboard_inputs = function() {
	up_press = keyboard_check(vk_up);
	space_pressed = keyboard_check_pressed(vk_space);
	interact_pressed = keyboard_check_pressed(ord("E"));
	x_dir = keyboard_check(vk_right) - keyboard_check(vk_left);
	y_dir = keyboard_check(ord("S")) - keyboard_check(ord("W"));
	grounded = place_meeting(x, y+1, obj_wall) || place_meeting(x, y+1, obj_rocket_slow);
}

/// @function					handle_x_speed();
handle_x_speed = function() {
	if (x_dir != 0) {
		//accelerate
		x_speed += x_dir*X_ACC;
		if (abs(x_speed) > x_speed_max) {
			x_speed = sign(x_speed)*x_speed_max;
		}
	}
	else {
		//deaccelerate to 0 speed
		if (abs(x_speed) > 0) {
			x_speed -= sign(x_speed)*X_ACC;
			if (abs(x_speed) < X_ACC) {
				x_speed = 0;
			}
		}
	}
}

/// @function					jump();
jump = function() {
	y_speed = -JUMP_SPEED;
	x_speed_max = WALK_SPEED_MAX_AIR;
	holding_up = true;
	
	obj_sound.play_sound_pitch(snd_jump, 0.5);

	
	alarm[0] = HOLD_TIME_MAX*room_speed;
}

/// @function					reset_max_speeds();
/// @desc						Reset x_speed_max and y_speed_max to their default values on ground.
reset_max_speeds = function() {
	rocket_jumping = false;
	y_speed_max = FALL_SPEED_MAX;
	x_speed_max = WALK_SPEED_MAX;
}

/// @function					set_white();
/// @desc						Draw the player completely white for [WHITE_TIME] seconds.
set_white = function() {
	draw_white = true;
	alarm[2] = WHITE_TIME*room_speed;
}