
switch (state) {
	case player_state.ALIVE:

		get_keyboard_inputs();
		set_x_facing();
		set_sprite();

		//change weapon
		if (keyboard_check_pressed(ord("Z"))) {
			rocket_index = (rocket_index == array_length(ROCKET_ARSENAL) - 1 ? 0 : rocket_index + 1);
		}

		//shoot
		if (space_pressed && can_shoot) {
			shoot();
		}
		
		//transition through door
		var door = instance_place(x, y, obj_door);
		if (door != noone && !door.locked && grounded && interact_pressed) {
			state = player_state.TRANSITIONING_ROOM;
			door.start_transition();
			break;
		}

		handle_x_speed();

		//player has been holding space but let go
		if (holding_up && !up_press) {
			holding_up = false;
			alarm[0] = -1;
		}

		//jumping and y speed
		if (up_press && grounded) {
			handle_dust_particles_jumping();
			jump();
		}
		else {
			y_speed += (holding_up && !rocket_jumping ? Y_ACC_HOLDING : Y_ACC);
			if (abs(y_speed) > y_speed_max) {
				y_speed = sign(y_speed)*y_speed_max;
			}
		}


		
		if (instance_place(x, y + y_speed, obj_rocket_slow) && y_speed >= 0) {
			
			while (!place_meeting(x, y + sign(y_speed), obj_rocket_slow)) {
				y += sign(y_speed);
			}
			y_speed = 0;
		}


		
		//collisions
		handle_x_collision();
		handle_y_collision();
		//add speeds to positions
		var rocket_speed = 0;
		if (instance_exists(rocket)) {
			if (place_meeting(x, y + 1, obj_rocket_slow) && abs(y + sprite_height - rocket.y) <= 1) {
				rocket_speed = rocket.spd;
			}
		}
		x += x_speed + rocket_speed;
		y += y_speed;

		//reset max speeds if grounded
		if (place_meeting(x, y+1, obj_wall)) {
			reset_max_speeds();
		}

		//particles
		handle_fire_particles();
		handle_dust_particles();
		
		break;
	case player_state.DEAD:
		y -= ANGEL_Y_SPEED;
		break;
	case player_state.TRANSITIONING_ROOM:
		break;
	default:
		break;
}

//set previous values
grounded_prev = grounded;
image_xscale_prev = image_xscale;