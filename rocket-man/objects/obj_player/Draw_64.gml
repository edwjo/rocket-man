/// @description ammo


var sub_image = (can_shoot ? 0 : 1);
var margin = 64;

draw_sprite_ext(
	spr_rocket_gui,
	sub_image,
	margin,
	margin,
	4,
	4,
	0,
	-1,
	1
);