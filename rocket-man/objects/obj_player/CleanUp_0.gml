/// @description particles

part_type_destroy(dust_part);
part_type_destroy(fire_part);

part_emitter_destroy(dust_system, dust_emitter);
part_emitter_destroy(fire_system, fire_emitter);

part_system_destroy(dust_system);
part_system_destroy(fire_system);