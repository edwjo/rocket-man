/// @description respawn
state = player_state.ALIVE;
x_speed = 0;
y_speed = 0;
image_alpha = 1;
if (spawn_point.x != -1 && spawn_point.y != -1) {
	if (!instance_exists(obj_respawn_reset)) {
		instance_create_layer(0, 0, "Instances", obj_respawn_reset);
	}
	obj_respawn_reset.reset();
	x = spawn_point.x;
	y = spawn_point.y;
}
else {
	log("ERROR: no spawn point set");
}