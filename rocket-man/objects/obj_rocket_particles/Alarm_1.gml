/// @description Increase burst amount

if (!instance_exists(rocket)) {
	burst_amount = 0;
}
else if (burst_amount < MAX_BURST_AMOUNT) {
	burst_amount++;
	alarm[1] = BURST_INCR_TIME*room_speed;
}
part_emitter_stream(dust_system, dust_emitter, dust_part, burst_amount);
part_emitter_stream(dust_system, fire_emitter, fire_part, burst_amount);