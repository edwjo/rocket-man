
var pos = rocket.get_particles_pos();
part_emitter_region(
	dust_system,
	dust_emitter,
	pos.x - EMITTER_RADIUS,
	pos.x + EMITTER_RADIUS,
	pos.y - EMITTER_RADIUS,
	pos.y + EMITTER_RADIUS,
	ps_shape_ellipse,
	ps_distr_gaussian
);
part_emitter_region(
	dust_system,
	fire_emitter,
	pos.x - EMITTER_RADIUS,
	pos.x + EMITTER_RADIUS,
	pos.y - EMITTER_RADIUS,
	pos.y + EMITTER_RADIUS,
	ps_shape_ellipse,
	ps_distr_gaussian
);
