EMITTER_RADIUS = 12;
MAX_BURST_AMOUNT = 10;
BURST_INCR_TIME = 0.02;
BURST_INIT_TIME = 0.1;

rocket = noone;
burst_amount = 0;

//particle
dust_system = part_system_create();
dust_emitter = part_emitter_create(dust_system);
fire_emitter = part_emitter_create(dust_system);
dust_part = part_type_create();
fire_part = part_type_create();

//smoke
part_type_sprite(dust_part, spr_dust_particle, false, false, false);
part_type_life(dust_part, 0.2*room_speed, 0.3*room_speed);
//part_type_alpha2(dust_part, 0.7, 0);
part_type_size(dust_part, 1, 2, 0.07, 0);
part_type_color_mix(dust_part, $777777, $CCCCCC);
part_type_direction(dust_part, 0, 359, 0, 0);
part_type_speed(dust_part, 0.5, 2, -0.01, 0);

//fire
part_type_sprite(fire_part, spr_dust_particle, false, false, false);
part_type_life(fire_part, 0.05*room_speed, 0.15*room_speed);
//part_type_alpha2(fire_part, 0.7, 0);
part_type_size(fire_part, 1, 2, 0.07, 0);
part_type_color_mix(fire_part, c_red, c_orange);
part_type_direction(fire_part, 0, 359, 0, 0);
part_type_speed(fire_part, 0.5, 5, -0.01, 0);





part_emitter_region(
	dust_system,
	fire_emitter,
	x + sprite_width/2 - EMITTER_RADIUS,
	x + sprite_width/2 + EMITTER_RADIUS,
	y + sprite_height/2 - EMITTER_RADIUS,
	y + sprite_height/2 + EMITTER_RADIUS,
	ps_shape_ellipse,
	ps_distr_gaussian
);
part_emitter_region(
	dust_system,
	dust_emitter,
	x + sprite_width/2 - EMITTER_RADIUS,
	x + sprite_width/2 + EMITTER_RADIUS,
	y + sprite_height/2 - EMITTER_RADIUS,
	y + sprite_height/2 + EMITTER_RADIUS,
	ps_shape_ellipse,
	ps_distr_gaussian
);

alarm[1] = BURST_INIT_TIME*room_speed;

begin_destruction = function() {
	alarm[0] = room_speed;
	part_emitter_stream(dust_system, dust_emitter, dust_part, 0);
	part_emitter_stream(dust_system, fire_emitter, fire_part, 0);
}