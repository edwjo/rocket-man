DESTRUCTION_TIME = 0.35;


deactivate_self = function() {
	instance_create_layer(x, y, "Instances", obj_destructible_block_part_system);
	instance_deactivate_object(self);
}