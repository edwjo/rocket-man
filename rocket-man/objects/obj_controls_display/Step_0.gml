var dist = abs(x - obj_player.x);
if (dist <= FULL_DISPLAY_RADIUS) {
	image_alpha = 1;
}
else if (FULL_DISPLAY_RADIUS < dist && dist <= FULL_DISPLAY_RADIUS + FADE_OUTER_RIM) {
	// 0 <= image_alpha <= 1
	image_alpha = 1 - (dist - FULL_DISPLAY_RADIUS)/FADE_OUTER_RIM;
}
else {
	image_alpha = 0;
}

