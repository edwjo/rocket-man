BLAST_RADIUS = 175;
MAX_SPEED_RADIUS = 30;
MAX_SPEED = 33;
alarm[0] = 1*room_speed;

var angle = point_direction(x, y, obj_player.get_center_x(), obj_player.get_center_y());
var distance = sqrt(sqr(x - obj_player.get_center_x()) + sqr(y - obj_player.get_center_y()));

obj_sound.play_sound(snd_explosion);

//check for player distance
if (distance <= BLAST_RADIUS) {
	if (obj_player.dead_on_explosion) {
		obj_player.die();
	}
	else {
		var x_speed = cos(degtorad(angle))*MAX_SPEED/1.5;
		var y_speed = -sin(degtorad(angle))*MAX_SPEED;
		obj_player.set_explosion_speed(x_speed, y_speed);
		obj_player.set_white();
	}
}
/*
else if (distance <= BLAST_RADIUS){
	var player_rel = distance - MAX_SPEED_RADIUS;
	var len = BLAST_RADIUS - MAX_SPEED_RADIUS;
	var pow = (len - player_rel)/len*MAX_SPEED;
	obj_player.set_explosion_speed(cos(degtorad(angle))*pow/2, -sin(degtorad(angle))*pow);
}*/

//collision with obj_door_trigger
var inst = collision_circle(x, y, BLAST_RADIUS, obj_door_trigger, false, true);
if (inst != noone) {
	inst.trigger();
}

//collision with obj_destructible_block
var block_ids = [];
//add all obj_destructible_block instances to array
for (var i = 0; i < instance_number(obj_destructible_block); i++) {
	block_ids[i] = instance_find(obj_destructible_block,i);
}
//destroy all within range
for (var i = 0; i < array_length(block_ids); i++) {
	var inst = collision_circle(x, y, BLAST_RADIUS-15, block_ids[i], false, true);
	if (inst != noone) {
		inst.deactivate_self();
	}
}