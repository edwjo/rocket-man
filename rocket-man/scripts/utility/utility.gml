/// @function					log(str)
/// @param		{any}	str		The string to log.
function log(str) {
	show_debug_message(string(str));
}