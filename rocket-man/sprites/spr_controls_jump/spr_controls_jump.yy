{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 127,
  "bbox_top": 0,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e03758a3-a521-43eb-a31a-d5edfb0b6cd0","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e03758a3-a521-43eb-a31a-d5edfb0b6cd0","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},"LayerId":{"name":"b7268f02-d624-4ec5-839b-6527217f94f9","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_controls_jump","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},"resourceVersion":"1.0","name":"e03758a3-a521-43eb-a31a-d5edfb0b6cd0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_controls_jump","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"596737ad-78ff-4807-a06b-0c3bdfbbb53a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e03758a3-a521-43eb-a31a-d5edfb0b6cd0","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_controls_jump","path":"sprites/spr_controls_jump/spr_controls_jump.yy",},
    "resourceVersion": "1.3",
    "name": "spr_controls_jump",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b7268f02-d624-4ec5-839b-6527217f94f9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Controls Display",
    "path": "folders/Controls Display.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_controls_jump",
  "tags": [],
  "resourceType": "GMSprite",
}