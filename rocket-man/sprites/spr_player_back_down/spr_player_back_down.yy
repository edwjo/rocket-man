{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 67,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 68,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":{"name":"68f835d5-622f-4394-bd71-d1d8121b6979","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_back_down","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f430bafc-8b5d-45fb-a7db-9c0523e8672d","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f430bafc-8b5d-45fb-a7db-9c0523e8672d","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"LayerId":{"name":"68f835d5-622f-4394-bd71-d1d8121b6979","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_back_down","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","name":"f430bafc-8b5d-45fb-a7db-9c0523e8672d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_back_down","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4363ef15-5702-4e7b-8b84-f4b1c88ab5f0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b9b1049-6a21-45ee-8d66-4d5bf9b20e61","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e7af6bf-584e-4910-9ee4-70761df51983","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f430bafc-8b5d-45fb-a7db-9c0523e8672d","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_back_down","path":"sprites/spr_player_back_down/spr_player_back_down.yy",},
    "resourceVersion": "1.3",
    "name": "spr_player_back_down",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"68f835d5-622f-4394-bd71-d1d8121b6979","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Animation Sprites",
    "path": "folders/Player/Animation Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_back_down",
  "tags": [],
  "resourceType": "GMSprite",
}