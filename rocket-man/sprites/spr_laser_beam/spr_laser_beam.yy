{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 31,
  "bbox_right": 32,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"92c8f98c-e5e9-42fc-9769-6bb68c165368","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92c8f98c-e5e9-42fc-9769-6bb68c165368","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"f6a65c42-1a82-4da0-b46f-dec2fc651782","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"92c8f98c-e5e9-42fc-9769-6bb68c165368","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"ebcd5a1c-ecc2-4a5d-8189-22692a1393ea","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"92c8f98c-e5e9-42fc-9769-6bb68c165368","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"f6a65c42-1a82-4da0-b46f-dec2fc651782","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"LayerId":{"name":"ebcd5a1c-ecc2-4a5d-8189-22692a1393ea","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7765745a-4daf-4d31-9b62-cf5ba19a76c4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92c8f98c-e5e9-42fc-9769-6bb68c165368","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"452070d5-de66-4fb2-bda8-3fdbe3d63b7a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb1cff3a-4567-4649-b8f4-7137ce18d2b2","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_laser_beam","path":"sprites/spr_laser_beam/spr_laser_beam.yy",},
    "resourceVersion": "1.3",
    "name": "spr_laser_beam",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":82.0,"displayName":"default","resourceVersion":"1.0","name":"f6a65c42-1a82-4da0-b46f-dec2fc651782","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":27.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"ebcd5a1c-ecc2-4a5d-8189-22692a1393ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Laser",
    "path": "folders/Obstacles/Laser.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_laser_beam",
  "tags": [],
  "resourceType": "GMSprite",
}