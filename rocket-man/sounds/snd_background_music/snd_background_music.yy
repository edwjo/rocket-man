{
  "compression": 1,
  "volume": 0.08,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "background_music",
    "path": "audiogroups/background_music",
  },
  "soundFile": "snd_background_music.wav",
  "duration": 270.550476,
  "parent": {
    "name": "rocket-man",
    "path": "rocket-man.yyp",
  },
  "resourceVersion": "1.0",
  "name": "snd_background_music",
  "tags": [],
  "resourceType": "GMSound",
}