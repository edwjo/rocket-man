{
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_powerup.wav",
  "duration": 2.004819,
  "parent": {
    "name": "Altar",
    "path": "folders/Altar.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_powerup",
  "tags": [],
  "resourceType": "GMSound",
}