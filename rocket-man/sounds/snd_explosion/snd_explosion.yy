{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_explosion.wav",
  "duration": 0.994048,
  "parent": {
    "name": "Rockets",
    "path": "folders/Player/Rockets.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_explosion",
  "tags": [],
  "resourceType": "GMSound",
}