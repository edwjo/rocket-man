{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_door_open.wav",
  "duration": 1.148146,
  "parent": {
    "name": "Door",
    "path": "folders/Door.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_door_open",
  "tags": [],
  "resourceType": "GMSound",
}