{
  "compression": 0,
  "volume": 0.28,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_death.wav",
  "duration": 0.864751,
  "parent": {
    "name": "Player",
    "path": "folders/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_death",
  "tags": [],
  "resourceType": "GMSound",
}