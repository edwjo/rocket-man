{
  "compression": 1,
  "volume": 0.71,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_checkpoint.wav",
  "duration": 0.451984,
  "parent": {
    "name": "Checkpoint",
    "path": "folders/Checkpoint.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_checkpoint",
  "tags": [],
  "resourceType": "GMSound",
}