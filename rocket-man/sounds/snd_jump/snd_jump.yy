{
  "compression": 1,
  "volume": 0.52,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "sound_effects",
    "path": "audiogroups/sound_effects",
  },
  "soundFile": "snd_jump.wav",
  "duration": 0.236179,
  "parent": {
    "name": "Player",
    "path": "folders/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_jump",
  "tags": [],
  "resourceType": "GMSound",
}