{
  "hinting": 1,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Orange Kid",
  "styleName": "Regular",
  "size": 35.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 0,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":9,"h":56,"character":32,"shift":9,"offset":0,},
    "33": {"x":111,"y":118,"w":4,"h":56,"character":33,"shift":9,"offset":3,},
    "34": {"x":98,"y":118,"w":11,"h":56,"character":34,"shift":13,"offset":1,},
    "35": {"x":78,"y":118,"w":18,"h":56,"character":35,"shift":22,"offset":2,},
    "36": {"x":58,"y":118,"w":18,"h":56,"character":36,"shift":22,"offset":2,},
    "37": {"x":25,"y":118,"w":31,"h":56,"character":37,"shift":35,"offset":2,},
    "38": {"x":2,"y":118,"w":21,"h":56,"character":38,"shift":25,"offset":2,},
    "39": {"x":488,"y":60,"w":4,"h":56,"character":39,"shift":6,"offset":1,},
    "40": {"x":475,"y":60,"w":11,"h":56,"character":40,"shift":16,"offset":3,},
    "41": {"x":462,"y":60,"w":11,"h":56,"character":41,"shift":16,"offset":2,},
    "42": {"x":117,"y":118,"w":11,"h":56,"character":42,"shift":15,"offset":2,},
    "43": {"x":442,"y":60,"w":18,"h":56,"character":43,"shift":22,"offset":2,},
    "44": {"x":417,"y":60,"w":7,"h":56,"character":44,"shift":11,"offset":2,},
    "45": {"x":404,"y":60,"w":11,"h":56,"character":45,"shift":13,"offset":1,},
    "46": {"x":398,"y":60,"w":4,"h":56,"character":46,"shift":8,"offset":2,},
    "47": {"x":382,"y":60,"w":14,"h":56,"character":47,"shift":18,"offset":2,},
    "48": {"x":366,"y":60,"w":14,"h":56,"character":48,"shift":18,"offset":2,},
    "49": {"x":357,"y":60,"w":7,"h":56,"character":49,"shift":11,"offset":2,},
    "50": {"x":341,"y":60,"w":14,"h":56,"character":50,"shift":18,"offset":2,},
    "51": {"x":325,"y":60,"w":14,"h":56,"character":51,"shift":18,"offset":2,},
    "52": {"x":305,"y":60,"w":18,"h":56,"character":52,"shift":20,"offset":1,},
    "53": {"x":426,"y":60,"w":14,"h":56,"character":53,"shift":18,"offset":2,},
    "54": {"x":130,"y":118,"w":14,"h":56,"character":54,"shift":18,"offset":2,},
    "55": {"x":146,"y":118,"w":14,"h":56,"character":55,"shift":18,"offset":2,},
    "56": {"x":162,"y":118,"w":14,"h":56,"character":56,"shift":18,"offset":2,},
    "57": {"x":17,"y":176,"w":14,"h":56,"character":57,"shift":18,"offset":2,},
    "58": {"x":11,"y":176,"w":4,"h":56,"character":58,"shift":8,"offset":2,},
    "59": {"x":2,"y":176,"w":7,"h":56,"character":59,"shift":11,"offset":2,},
    "60": {"x":494,"y":118,"w":11,"h":56,"character":60,"shift":15,"offset":2,},
    "61": {"x":474,"y":118,"w":18,"h":56,"character":61,"shift":22,"offset":2,},
    "62": {"x":461,"y":118,"w":11,"h":56,"character":62,"shift":15,"offset":2,},
    "63": {"x":445,"y":118,"w":14,"h":56,"character":63,"shift":18,"offset":2,},
    "64": {"x":418,"y":118,"w":25,"h":56,"character":64,"shift":29,"offset":2,},
    "65": {"x":395,"y":118,"w":21,"h":56,"character":65,"shift":25,"offset":2,},
    "66": {"x":375,"y":118,"w":18,"h":56,"character":66,"shift":22,"offset":2,},
    "67": {"x":355,"y":118,"w":18,"h":56,"character":67,"shift":22,"offset":2,},
    "68": {"x":335,"y":118,"w":18,"h":56,"character":68,"shift":22,"offset":2,},
    "69": {"x":319,"y":118,"w":14,"h":56,"character":69,"shift":18,"offset":2,},
    "70": {"x":303,"y":118,"w":14,"h":56,"character":70,"shift":17,"offset":2,},
    "71": {"x":283,"y":118,"w":18,"h":56,"character":71,"shift":22,"offset":2,},
    "72": {"x":263,"y":118,"w":18,"h":56,"character":72,"shift":22,"offset":2,},
    "73": {"x":257,"y":118,"w":4,"h":56,"character":73,"shift":8,"offset":2,},
    "74": {"x":241,"y":118,"w":14,"h":56,"character":74,"shift":17,"offset":1,},
    "75": {"x":221,"y":118,"w":18,"h":56,"character":75,"shift":21,"offset":2,},
    "76": {"x":205,"y":118,"w":14,"h":56,"character":76,"shift":17,"offset":2,},
    "77": {"x":178,"y":118,"w":25,"h":56,"character":77,"shift":29,"offset":2,},
    "78": {"x":285,"y":60,"w":18,"h":56,"character":78,"shift":22,"offset":2,},
    "79": {"x":265,"y":60,"w":18,"h":56,"character":79,"shift":22,"offset":2,},
    "80": {"x":245,"y":60,"w":18,"h":56,"character":80,"shift":22,"offset":2,},
    "81": {"x":379,"y":2,"w":18,"h":56,"character":81,"shift":22,"offset":2,},
    "82": {"x":350,"y":2,"w":18,"h":56,"character":82,"shift":22,"offset":2,},
    "83": {"x":330,"y":2,"w":18,"h":56,"character":83,"shift":22,"offset":2,},
    "84": {"x":310,"y":2,"w":18,"h":56,"character":84,"shift":20,"offset":1,},
    "85": {"x":290,"y":2,"w":18,"h":56,"character":85,"shift":22,"offset":2,},
    "86": {"x":267,"y":2,"w":21,"h":56,"character":86,"shift":25,"offset":2,},
    "87": {"x":240,"y":2,"w":25,"h":56,"character":87,"shift":29,"offset":2,},
    "88": {"x":220,"y":2,"w":18,"h":56,"character":88,"shift":22,"offset":2,},
    "89": {"x":200,"y":2,"w":18,"h":56,"character":89,"shift":22,"offset":2,},
    "90": {"x":184,"y":2,"w":14,"h":56,"character":90,"shift":18,"offset":2,},
    "91": {"x":370,"y":2,"w":7,"h":56,"character":91,"shift":11,"offset":2,},
    "92": {"x":168,"y":2,"w":14,"h":56,"character":92,"shift":18,"offset":2,},
    "93": {"x":146,"y":2,"w":7,"h":56,"character":93,"shift":11,"offset":2,},
    "94": {"x":126,"y":2,"w":18,"h":56,"character":94,"shift":22,"offset":2,},
    "95": {"x":106,"y":2,"w":18,"h":56,"character":95,"shift":22,"offset":2,},
    "96": {"x":97,"y":2,"w":7,"h":56,"character":96,"shift":11,"offset":3,},
    "97": {"x":77,"y":2,"w":18,"h":56,"character":97,"shift":22,"offset":2,},
    "98": {"x":61,"y":2,"w":14,"h":56,"character":98,"shift":18,"offset":2,},
    "99": {"x":45,"y":2,"w":14,"h":56,"character":99,"shift":18,"offset":2,},
    "100": {"x":29,"y":2,"w":14,"h":56,"character":100,"shift":18,"offset":2,},
    "101": {"x":13,"y":2,"w":14,"h":56,"character":101,"shift":18,"offset":2,},
    "102": {"x":155,"y":2,"w":11,"h":56,"character":102,"shift":14,"offset":2,},
    "103": {"x":399,"y":2,"w":14,"h":56,"character":103,"shift":18,"offset":2,},
    "104": {"x":65,"y":60,"w":14,"h":56,"character":104,"shift":18,"offset":2,},
    "105": {"x":415,"y":2,"w":4,"h":56,"character":105,"shift":8,"offset":2,},
    "106": {"x":220,"y":60,"w":7,"h":56,"character":106,"shift":10,"offset":1,},
    "107": {"x":204,"y":60,"w":14,"h":56,"character":107,"shift":18,"offset":2,},
    "108": {"x":198,"y":60,"w":4,"h":56,"character":108,"shift":8,"offset":2,},
    "109": {"x":171,"y":60,"w":25,"h":56,"character":109,"shift":29,"offset":2,},
    "110": {"x":155,"y":60,"w":14,"h":56,"character":110,"shift":18,"offset":2,},
    "111": {"x":139,"y":60,"w":14,"h":56,"character":111,"shift":18,"offset":2,},
    "112": {"x":123,"y":60,"w":14,"h":56,"character":112,"shift":18,"offset":2,},
    "113": {"x":107,"y":60,"w":14,"h":56,"character":113,"shift":18,"offset":2,},
    "114": {"x":94,"y":60,"w":11,"h":56,"character":114,"shift":14,"offset":2,},
    "115": {"x":229,"y":60,"w":14,"h":56,"character":115,"shift":17,"offset":2,},
    "116": {"x":81,"y":60,"w":11,"h":56,"character":116,"shift":13,"offset":1,},
    "117": {"x":49,"y":60,"w":14,"h":56,"character":117,"shift":18,"offset":2,},
    "118": {"x":29,"y":60,"w":18,"h":56,"character":118,"shift":20,"offset":1,},
    "119": {"x":2,"y":60,"w":25,"h":56,"character":119,"shift":27,"offset":1,},
    "120": {"x":485,"y":2,"w":14,"h":56,"character":120,"shift":18,"offset":2,},
    "121": {"x":469,"y":2,"w":14,"h":56,"character":121,"shift":18,"offset":2,},
    "122": {"x":453,"y":2,"w":14,"h":56,"character":122,"shift":17,"offset":1,},
    "123": {"x":440,"y":2,"w":11,"h":56,"character":123,"shift":15,"offset":2,},
    "124": {"x":434,"y":2,"w":4,"h":56,"character":124,"shift":11,"offset":4,},
    "125": {"x":421,"y":2,"w":11,"h":56,"character":125,"shift":15,"offset":2,},
    "126": {"x":33,"y":176,"w":18,"h":56,"character":126,"shift":22,"offset":2,},
    "9647": {"x":53,"y":176,"w":10,"h":56,"character":9647,"shift":28,"offset":9,},
  },
  "kerningPairs": [
    {"first":34,"second":65,"amount":-3,},
    {"first":34,"second":74,"amount":-3,},
    {"first":34,"second":193,"amount":-3,},
    {"first":34,"second":194,"amount":-3,},
    {"first":34,"second":196,"amount":-3,},
    {"first":34,"second":197,"amount":-3,},
    {"first":39,"second":65,"amount":-3,},
    {"first":39,"second":74,"amount":-3,},
    {"first":39,"second":193,"amount":-3,},
    {"first":39,"second":194,"amount":-3,},
    {"first":39,"second":196,"amount":-3,},
    {"first":39,"second":197,"amount":-3,},
    {"first":44,"second":49,"amount":-2,},
    {"first":44,"second":52,"amount":-3,},
    {"first":44,"second":55,"amount":-1,},
    {"first":44,"second":84,"amount":-3,},
    {"first":44,"second":86,"amount":-4,},
    {"first":44,"second":87,"amount":-4,},
    {"first":44,"second":89,"amount":-4,},
    {"first":44,"second":376,"amount":-4,},
    {"first":45,"second":84,"amount":-3,},
    {"first":45,"second":86,"amount":-1,},
    {"first":45,"second":87,"amount":-1,},
    {"first":45,"second":89,"amount":-1,},
    {"first":45,"second":376,"amount":-1,},
    {"first":46,"second":49,"amount":-2,},
    {"first":46,"second":52,"amount":-3,},
    {"first":46,"second":55,"amount":-1,},
    {"first":46,"second":84,"amount":-3,},
    {"first":46,"second":86,"amount":-4,},
    {"first":46,"second":87,"amount":-4,},
    {"first":46,"second":89,"amount":-4,},
    {"first":46,"second":376,"amount":-4,},
    {"first":52,"second":44,"amount":-2,},
    {"first":52,"second":46,"amount":-2,},
    {"first":52,"second":55,"amount":-1,},
    {"first":52,"second":8218,"amount":-2,},
    {"first":52,"second":8222,"amount":-2,},
    {"first":52,"second":8230,"amount":-2,},
    {"first":55,"second":44,"amount":-3,},
    {"first":55,"second":46,"amount":-3,},
    {"first":55,"second":52,"amount":-1,},
    {"first":55,"second":8218,"amount":-3,},
    {"first":55,"second":8222,"amount":-3,},
    {"first":55,"second":8230,"amount":-3,},
    {"first":65,"second":34,"amount":-3,},
    {"first":65,"second":39,"amount":-3,},
    {"first":65,"second":84,"amount":-3,},
    {"first":65,"second":86,"amount":-2,},
    {"first":65,"second":87,"amount":-2,},
    {"first":65,"second":89,"amount":-2,},
    {"first":65,"second":376,"amount":-2,},
    {"first":65,"second":8216,"amount":-3,},
    {"first":65,"second":8217,"amount":-3,},
    {"first":65,"second":8220,"amount":-3,},
    {"first":65,"second":8221,"amount":-3,},
    {"first":70,"second":44,"amount":-4,},
    {"first":70,"second":45,"amount":-1,},
    {"first":70,"second":46,"amount":-4,},
    {"first":70,"second":47,"amount":-4,},
    {"first":70,"second":65,"amount":-3,},
    {"first":70,"second":74,"amount":-3,},
    {"first":70,"second":171,"amount":-1,},
    {"first":70,"second":187,"amount":-1,},
    {"first":70,"second":193,"amount":-3,},
    {"first":70,"second":194,"amount":-3,},
    {"first":70,"second":196,"amount":-3,},
    {"first":70,"second":197,"amount":-3,},
    {"first":70,"second":198,"amount":-4,},
    {"first":70,"second":8211,"amount":-1,},
    {"first":70,"second":8212,"amount":-1,},
    {"first":70,"second":8218,"amount":-4,},
    {"first":70,"second":8222,"amount":-4,},
    {"first":70,"second":8230,"amount":-4,},
    {"first":70,"second":8249,"amount":-1,},
    {"first":70,"second":8250,"amount":-1,},
    {"first":75,"second":86,"amount":-1,},
    {"first":75,"second":87,"amount":-1,},
    {"first":75,"second":89,"amount":-1,},
    {"first":75,"second":376,"amount":-1,},
    {"first":76,"second":34,"amount":-3,},
    {"first":76,"second":39,"amount":-3,},
    {"first":76,"second":45,"amount":-4,},
    {"first":76,"second":84,"amount":-3,},
    {"first":76,"second":86,"amount":-3,},
    {"first":76,"second":87,"amount":-3,},
    {"first":76,"second":89,"amount":-3,},
    {"first":76,"second":171,"amount":-4,},
    {"first":76,"second":187,"amount":-4,},
    {"first":76,"second":376,"amount":-3,},
    {"first":76,"second":8211,"amount":-4,},
    {"first":76,"second":8212,"amount":-4,},
    {"first":76,"second":8216,"amount":-3,},
    {"first":76,"second":8217,"amount":-3,},
    {"first":76,"second":8220,"amount":-3,},
    {"first":76,"second":8221,"amount":-3,},
    {"first":76,"second":8249,"amount":-4,},
    {"first":76,"second":8250,"amount":-4,},
    {"first":80,"second":44,"amount":-4,},
    {"first":80,"second":45,"amount":-1,},
    {"first":80,"second":46,"amount":-4,},
    {"first":80,"second":47,"amount":-4,},
    {"first":80,"second":65,"amount":-3,},
    {"first":80,"second":74,"amount":-3,},
    {"first":80,"second":171,"amount":-1,},
    {"first":80,"second":187,"amount":-1,},
    {"first":80,"second":193,"amount":-3,},
    {"first":80,"second":194,"amount":-3,},
    {"first":80,"second":196,"amount":-3,},
    {"first":80,"second":197,"amount":-3,},
    {"first":80,"second":198,"amount":-4,},
    {"first":80,"second":8211,"amount":-1,},
    {"first":80,"second":8212,"amount":-1,},
    {"first":80,"second":8218,"amount":-4,},
    {"first":80,"second":8222,"amount":-4,},
    {"first":80,"second":8230,"amount":-4,},
    {"first":80,"second":8249,"amount":-1,},
    {"first":80,"second":8250,"amount":-1,},
    {"first":84,"second":44,"amount":-4,},
    {"first":84,"second":45,"amount":-3,},
    {"first":84,"second":46,"amount":-4,},
    {"first":84,"second":65,"amount":-3,},
    {"first":84,"second":74,"amount":-3,},
    {"first":84,"second":171,"amount":-3,},
    {"first":84,"second":187,"amount":-3,},
    {"first":84,"second":193,"amount":-3,},
    {"first":84,"second":194,"amount":-3,},
    {"first":84,"second":196,"amount":-3,},
    {"first":84,"second":197,"amount":-3,},
    {"first":84,"second":198,"amount":-3,},
    {"first":84,"second":8211,"amount":-3,},
    {"first":84,"second":8212,"amount":-3,},
    {"first":84,"second":8218,"amount":-4,},
    {"first":84,"second":8222,"amount":-4,},
    {"first":84,"second":8230,"amount":-4,},
    {"first":84,"second":8249,"amount":-3,},
    {"first":84,"second":8250,"amount":-3,},
    {"first":86,"second":44,"amount":-4,},
    {"first":86,"second":45,"amount":-1,},
    {"first":86,"second":46,"amount":-4,},
    {"first":86,"second":47,"amount":-3,},
    {"first":86,"second":65,"amount":-2,},
    {"first":86,"second":74,"amount":-3,},
    {"first":86,"second":171,"amount":-1,},
    {"first":86,"second":187,"amount":-1,},
    {"first":86,"second":193,"amount":-2,},
    {"first":86,"second":194,"amount":-2,},
    {"first":86,"second":196,"amount":-2,},
    {"first":86,"second":197,"amount":-2,},
    {"first":86,"second":198,"amount":-3,},
    {"first":86,"second":8211,"amount":-1,},
    {"first":86,"second":8212,"amount":-1,},
    {"first":86,"second":8218,"amount":-4,},
    {"first":86,"second":8222,"amount":-4,},
    {"first":86,"second":8230,"amount":-4,},
    {"first":86,"second":8249,"amount":-1,},
    {"first":86,"second":8250,"amount":-1,},
    {"first":87,"second":44,"amount":-4,},
    {"first":87,"second":45,"amount":-1,},
    {"first":87,"second":46,"amount":-4,},
    {"first":87,"second":47,"amount":-3,},
    {"first":87,"second":65,"amount":-2,},
    {"first":87,"second":74,"amount":-3,},
    {"first":87,"second":171,"amount":-1,},
    {"first":87,"second":187,"amount":-1,},
    {"first":87,"second":193,"amount":-2,},
    {"first":87,"second":194,"amount":-2,},
    {"first":87,"second":196,"amount":-2,},
    {"first":87,"second":197,"amount":-2,},
    {"first":87,"second":198,"amount":-3,},
    {"first":87,"second":8211,"amount":-1,},
    {"first":87,"second":8212,"amount":-1,},
    {"first":87,"second":8218,"amount":-4,},
    {"first":87,"second":8222,"amount":-4,},
    {"first":87,"second":8230,"amount":-4,},
    {"first":87,"second":8249,"amount":-1,},
    {"first":87,"second":8250,"amount":-1,},
    {"first":89,"second":44,"amount":-4,},
    {"first":89,"second":45,"amount":-1,},
    {"first":89,"second":46,"amount":-4,},
    {"first":89,"second":47,"amount":-3,},
    {"first":89,"second":65,"amount":-2,},
    {"first":89,"second":74,"amount":-3,},
    {"first":89,"second":171,"amount":-1,},
    {"first":89,"second":187,"amount":-1,},
    {"first":89,"second":193,"amount":-2,},
    {"first":89,"second":194,"amount":-2,},
    {"first":89,"second":196,"amount":-2,},
    {"first":89,"second":197,"amount":-2,},
    {"first":89,"second":198,"amount":-3,},
    {"first":89,"second":8211,"amount":-1,},
    {"first":89,"second":8212,"amount":-1,},
    {"first":89,"second":8218,"amount":-4,},
    {"first":89,"second":8222,"amount":-4,},
    {"first":89,"second":8230,"amount":-4,},
    {"first":89,"second":8249,"amount":-1,},
    {"first":89,"second":8250,"amount":-1,},
  ],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Altar",
    "path": "folders/Altar.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_power_up",
  "tags": [],
  "resourceType": "GMFont",
}