{
  "isDnd": false,
  "volume": 1.0,
  "parentRoom": null,
  "views": [
    {"inherit":false,"visible":true,"xview":0,"yview":0,"wview":1920,"hview":1080,"xport":0,"yport":0,"wport":960,"hport":540,"hborder":960,"vborder":300,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
  ],
  "layers": [
    {"instances":[
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_out_of_bounds","path":"objects/obj_out_of_bounds/obj_out_of_bounds.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":160.0,"scaleY":2.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":-2496.0,"y":2240.0,"resourceVersion":"1.0","name":"inst_70E5F98C","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_parallax_scrolling","path":"objects/obj_parallax_scrolling/obj_parallax_scrolling.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":896.0,"y":576.0,"resourceVersion":"1.0","name":"inst_52FBAA32","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_spikes","path":"objects/obj_spikes/obj_spikes.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1088.0,"y":1344.0,"resourceVersion":"1.0","name":"inst_C69D6E4","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_spikes","path":"objects/obj_spikes/obj_spikes.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":1344.0,"resourceVersion":"1.0","name":"inst_6410A1F5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_spikes","path":"objects/obj_spikes/obj_spikes.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1216.0,"y":1344.0,"resourceVersion":"1.0","name":"inst_31818B8E","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_altar","path":"objects/obj_altar/obj_altar.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":2176.0,"y":1664.0,"resourceVersion":"1.0","name":"inst_51DE2DE5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_checkpoint","path":"objects/obj_checkpoint/obj_checkpoint.yy",},"inheritCode":false,"hasCreationCode":true,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":1216.0,"resourceVersion":"1.0","name":"inst_F3F0322","tags":[],"resourceType":"GMRInstance",},
      ],"visible":true,"depth":0,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":64,"gridY":64,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"Instances","tags":[],"resourceType":"GMRInstanceLayer",},
    {"instances":[],"visible":true,"depth":100,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"Background","tags":[],"resourceType":"GMRInstanceLayer",},
    {"instances":[
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":7.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":960.0,"y":896.0,"resourceVersion":"1.0","name":"inst_297B9CFF","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":7.0,"scaleY":16.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":192.0,"y":1280.0,"resourceVersion":"1.0","name":"inst_58552EE1","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":4.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":192.0,"y":640.0,"resourceVersion":"1.0","name":"inst_11D75F4A","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":11.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1664.0,"y":1728.0,"resourceVersion":"1.0","name":"inst_7402586E","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":3.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1088.0,"y":1408.0,"resourceVersion":"1.0","name":"inst_7DABA9B4","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":6.0,"scaleY":8.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1280.0,"y":1280.0,"resourceVersion":"1.0","name":"inst_1DBC7998","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":4.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":2304.0,"y":1472.0,"resourceVersion":"1.0","name":"inst_3E44E0F3","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":5.0,"scaleY":25.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1792.0,"y":0.0,"resourceVersion":"1.0","name":"inst_12FE85D9","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":3.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":2112.0,"y":1472.0,"resourceVersion":"1.0","name":"inst_6CD4B0CD","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":38.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":-64.0,"y":-64.0,"resourceVersion":"1.0","name":"inst_5AD164E8","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"obj_wall","path":"objects/obj_wall/obj_wall.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":4.0,"scaleY":17.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":1280.0,"resourceVersion":"1.0","name":"inst_1B0C9B3D","tags":[],"resourceType":"GMRInstance",},
        {"properties":[
            {"propertyId":{"name":"dest_pos","path":"objects/obj_door/obj_door.yy",},"objectId":{"name":"obj_door","path":"objects/obj_door/obj_door.yy",},"value":"new Vector2(512, 3584)","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
            {"propertyId":{"name":"dest_room","path":"objects/obj_door/obj_door.yy",},"objectId":{"name":"obj_door","path":"objects/obj_door/obj_door.yy",},"value":"rm_5","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
          ],"isDnd":false,"objectId":{"name":"obj_door","path":"objects/obj_door/obj_door.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":512.0,"resourceVersion":"1.0","name":"inst_C8FCB2C","tags":[],"resourceType":"GMRInstance",},
      ],"visible":true,"depth":200,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":64,"gridY":64,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"Walls","tags":[],"resourceType":"GMRInstanceLayer",},
    {"tilesetId":{"name":"ts","path":"tilesets/ts/ts.yy",},"x":0,"y":0,"tiles":{"SerialiseWidth":47,"SerialiseHeight":47,"TileSerialiseData":[
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,0,0,0,0,0,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,8,8,8,8,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,23,23,23,23,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,0,0,0,0,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,23,23,23,23,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,49,50,50,51,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,0,0,0,0,0,0,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,49,50,50,50,50,50,51,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,25,23,9,23,25,23,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,25,23,9,23,25,23,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,7,7,7,7,0,0,0,0,0,0,0,0,23,25,23,9,23,25,23,0,0,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,25,23,9,23,25,23,2147483648,0,0,2147483648,2147483648,2147483648,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,2147483648,2147483648,7,7,7,7,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,23,25,23,9,23,25,23,2147483648,0,2147483648,2147483648,2147483648,2147483648,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,1,2,2,2,2,2,3,0,0,0,1,2,2,3,23,9,23,1,2,2,2,2,3,8,8,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,19,23,9,23,17,18,18,18,18,19,8,23,17,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,21,2,2,2,22,18,18,18,18,19,8,23,17,16,18,16,18,16,18,16,18,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,19,8,23,17,18,18,18,5,34,34,34,6,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,19,23,23,33,34,34,34,35,24,23,24,17,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,19,23,23,23,23,23,23,23,23,23,23,17,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,19,23,23,23,23,23,23,23,23,23,23,17,16,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,21,2,2,2,2,2,2,2,2,2,2,22,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
2147483648,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
0,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
0,0,0,17,16,18,16,18,16,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
0,0,0,17,18,18,18,18,18,19,0,0,0,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,0,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
],},"visible":true,"depth":300,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"Tiles_1","tags":[],"resourceType":"GMRTileLayer",},
    {"spriteId":{"name":"Sprite34","path":"sprites/Sprite34/Sprite34.yy",},"colour":4294967295,"x":0,"y":0,"htiled":true,"vtiled":false,"hspeed":0.0,"vspeed":0.0,"stretch":false,"animationFPS":15.0,"animationSpeedType":0,"userdefinedAnimFPS":false,"visible":true,"depth":400,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"bg_near","tags":[],"resourceType":"GMRBackgroundLayer",},
    {"spriteId":{"name":"Sprite3435","path":"sprites/Sprite3435/Sprite3435.yy",},"colour":4294967295,"x":0,"y":0,"htiled":true,"vtiled":false,"hspeed":0.0,"vspeed":0.0,"stretch":false,"animationFPS":15.0,"animationSpeedType":0,"userdefinedAnimFPS":false,"visible":true,"depth":500,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"bg_far","tags":[],"resourceType":"GMRBackgroundLayer",},
    {"spriteId":{"name":"spr_bg_sunset","path":"sprites/spr_bg_sunset/spr_bg_sunset.yy",},"colour":4294967295,"x":0,"y":0,"htiled":false,"vtiled":false,"hspeed":0.0,"vspeed":0.0,"stretch":false,"animationFPS":15.0,"animationSpeedType":0,"userdefinedAnimFPS":false,"visible":true,"depth":600,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"resourceVersion":"1.0","name":"bg","tags":[],"resourceType":"GMRBackgroundLayer",},
  ],
  "inheritLayers": false,
  "creationCodeFile": "${project_dir}/rooms/Room1/RoomCreationCode.gml",
  "inheritCode": false,
  "instanceCreationOrder": [
    {"name":"inst_52FBAA32","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_297B9CFF","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_58552EE1","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_11D75F4A","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_7402586E","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_7DABA9B4","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_1DBC7998","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_C69D6E4","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_6410A1F5","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_31818B8E","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_51DE2DE5","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_3E44E0F3","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_12FE85D9","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_6CD4B0CD","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_5AD164E8","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_70E5F98C","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_1B0C9B3D","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_C8FCB2C","path":"rooms/rm_4/rm_4.yy",},
    {"name":"inst_F3F0322","path":"rooms/rm_4/rm_4.yy",},
  ],
  "inheritCreationOrder": false,
  "sequenceId": null,
  "roomSettings": {
    "inheritRoomSettings": false,
    "Width": 3000,
    "Height": 3000,
    "persistent": false,
  },
  "viewSettings": {
    "inheritViewSettings": false,
    "enableViews": true,
    "clearViewBackground": false,
    "clearDisplayBuffer": true,
  },
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "parent": {
    "name": "Rooms",
    "path": "folders/Rooms.yy",
  },
  "resourceVersion": "1.0",
  "name": "rm_4",
  "tags": [],
  "resourceType": "GMRoom",
}